const express = require('express');
const UsersService = require('../../services/UsersService');
const { successCallback, errorCallback } = require('../../helpers/withAsync');

const router = express.Router();
const usersService = new UsersService();

module.exports = () => {
  // 1
  router.post('/register', async (req, res) => {
    try {
      await usersService.register(req.body);
      successCallback(res, { message: 'A user was successfully registered.' });
    } catch (error) {
      errorCallback(res, error);
    }
  });

  // 2
  router.post('/login', async (req, res) => {
    try {
      const { token, user } = await usersService.login(req.body);
      successCallback(res, { message: 'A user was successfully logged in.', token, user });
    } catch (error) {
      errorCallback(res, error);
    }
  });

  return router;
};
