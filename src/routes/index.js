const express = require('express');

const router = express.Router();

const tasksRoute = require('./tasks/tasks');
const authRoute = require('./auth/auth');

module.exports = () => {
  router.use('/tasks', tasksRoute());
  router.use('/auth', authRoute());

  return router;
};
