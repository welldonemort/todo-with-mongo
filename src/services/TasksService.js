/* eslint-disable class-methods-use-this */
const TaskModel = require('../models/TaskModel');

class TasksService {
  /**
   * Get task
   * @param {string} title Task title
   */
  async getTaskByTitle(title) {
    return TaskModel.findOne({ title });
  }

  /**
   * Get list of users tasks
   * @param {string} userId User ID
   * @param {boolean} done Status of a task
   */
  async getUserTasks(userId, done = false) {
    return TaskModel.find({ userId, done });
  }

  /**
   * Create task for a user
   * @param {*} params Params of a task
   */
  async createTask(params) {
    const task = new TaskModel(params);
    return task.save();
  }

  /**
   * Edit task
   * @param {string} title Title of a task
   * @param {*} params Params of a task
   */
  async updateTaskByTitle(title, params) {
    return TaskModel.findOneAndUpdate({ title }, { $set: params }, { new: true });
  }

  /**
   * Delete task
   * @param {string} taskTitle Title of a task
   */
  async deleteTaskByTitle(title) {
    return TaskModel.findOneAndDelete({ title });
  }
}

module.exports = TasksService;
