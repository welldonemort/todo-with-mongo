/* eslint-disable class-methods-use-this */
const jwt = require('jsonwebtoken');
const UserModel = require('../models/UserModel');

class UsersService {
  /**
   * Login user
   * @param {string} email User email
   * @param {string} password User password
   */
  async login(payload) {
    const { email, password } = payload;
    const user = await UserModel.findOne({ email });

    // validations
    if (!user) {
      throw new Error('Invalid credentials');
    }
    if (!password) {
      throw new Error('Password is required!');
    }
    const isPasswordValid = await user.comparePassword(password);
    if (!isPasswordValid) {
      throw new Error('Invalid credentials');
    }

    const { _id, __v, password: _, ...userWithoutPassword } = user.toObject(); // Создаем новый объект пользователя без поля password

    const token = jwt.sign({ userId: user._id }, 'your-secret-key', { expiresIn: '10h' });

    return { token, user: userWithoutPassword };
  }

  /**
   * Register user
   * @param {string} email User email
   * @param {string} password User password
   * @param {string} firstName User first name
   * @param {string} lastName User last name
   */
  async register(payload) {
    const { email, password, firstName, lastName } = payload;

    const user = new UserModel({ email, password, firstName, lastName });
    const savedUser = await user.save();
    return savedUser;
  }
}

module.exports = UsersService;
