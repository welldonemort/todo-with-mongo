const checkAuth = (req, res, next) => {
  if (req.user) {
    return next();
  }
  return res.status(401).json({ message: 'Please login to make this request!' });
};

module.exports = {
  checkAuth,
};
