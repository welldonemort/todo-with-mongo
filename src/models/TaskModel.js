const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      index: { unique: true },
      minLength: 3,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      minLength: 8,
    },
    done: {
      type: Boolean,
      required: true,
    },
    userId: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
    },
  },
  {
    // автоматически добавляем
    // createdAt и updatedAt
    timestamps: true,
  }
);

module.exports = mongoose.model('Task', TaskSchema);
