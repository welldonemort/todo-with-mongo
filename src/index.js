const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const auth = require('./lib/auth');

const app = express();
const routes = require('./routes');

const PORT = 3000;

// middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(auth.initialize);

mongoose.connect('mongodb://127.0.0.1/todo');
const db = mongoose.connection;
db.on('error', (error) => console.log(error));
db.once('open', () => {
  console.log('Database Connected...');
});

app.use('/api', routes());

app.use((err, req, res, next) => {
  console.error(err);
  const { message } = err;
  const status = err.status || 500;
  return res.json({ status, message });
});

app.listen(PORT, () => console.log(`Express server listening on port http://localhost:${PORT}!`));
